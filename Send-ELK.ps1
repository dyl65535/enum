$elk_baddr = "https://172.16.3.5:9200"
# for when i eventually put https on Elasticsearch 
function Get-ElkHealth{
    param(
        $Token
    )
    $header = @{"Authorization"="Basic $Token"}
    $result = Invoke-WebRequest -Headers $header -Uri "$elk_baddr/_cluster/health" -Method Get
    $result = $result.Content | ConvertFrom-Json
    $result
}

function New-ElkIndex{
    param(
        $Token,
        $IndexName
    )
    $header = @{"Authorization"="Basic $Token"}
    $result = Invoke-WebRequest -Headers $header -Uri "$elk_baddr/$IndexName" -Method Put
    $result = $result.Content | ConvertFrom-Json
    $result
}

function New-ElkEvent{
    param(
        $Token,
        $IndexName,
        $Data
    )
    $header = @{
        "Authorization"= "Basic $Token" 
        "Content-Type" = "application/json"
    }

    if (-Not $Data.ContainsKey("timestamp")){
        $Data.Add("timestamp", (Get-Date -Format "yyyy-MM-ddTHH:mm:ssK"))
    }
    $Data = $Data | Convertto-Json -Depth 40 -Compress

    $result = Invoke-RestMethod -Headers $header -Uri "$elk_baddr/$IndexName/_doc/" -Method Post -Body $Data
    $result
}

add-type @"
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    public class TrustAllCertsPolicy : ICertificatePolicy {
        public bool CheckValidationResult(
            ServicePoint srvPoint, X509Certificate certificate,
            WebRequest request, int certificateProblem) {
            return true;
        }
    }
"@
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy

# $token = ""
# Get-ElkHealth -Token $token
#New-ElkIndex -Token $token -IndexName "enum_data_1"




