function Get-DomainEnum {
    param(
        [Parameter()]
        [ValidateNotNull()]
        $DC,
        [System.Management.Automation.PSCredential]
        [System.Management.Automation.Credential()]
        $Credential = [System.Management.Automation.PSCredential]::Empty
    )
    Import-Module ActiveDirectory
    
    $results = @{
        EnumType = "Domain"
        ADUsers = @{}
        ADComputers = @{}
        ADTrusts = @{}
        DomainController = @{}
        Domain = @{}
        Count = @{
            DomainAdmins = 0
        }
    }

    $results.ADUsers = Get-ADUser -Filter * -Server $DC -Credential $Credential | Select-Object DistinguishedName,Name,ObjectClass,SamAccountName,UserPrincipalName
    $results.ADComputers = Get-ADComputer -Filter * -Server $DC -Credential $Credential | Select-Object DistinguishedName,DNSHostName,Name,ObjectClass,SamAccountName
    $results.ADTrusts = Get-ADTrust -Filter * -Server $DC -Credential $Credential
    $results.DomainController = Get-ADDomainController -Filter * -Server $DC -Credential $Credential
    $results.Domain = Get-ADDomain -Server $DC -Credential $Credential | Select-Object Name,NetBIOSName,PDCEmulator,RIDMaster,DistinguishedName,DNSRoot,DomainControllersContainer,DomainMode,Forest,InfrastructureMaster 
    
    $domadmins = Get-ADGroupMember "domain admins" -Server $DC -Credential $Credential  | Select-Object Name,SamAccountName
    $results.DomainAdmins = $domadmins
    $results.Count.DomainAdmins = $domadmins.Count


    
    return $results

}

Import-Module .\Send-ELK.ps1

#creds need specific format, and the DC parameter needs to be FQDN.
$creds = Get-Credential "dyl_adds@secn3t.ad"     
$enum = Get-DomainEnum -Credential $creds -DC dc01.secn3t.ad

try{
    New-ElkEvent -IndexName testenum -Data $enum | Out-Null
    Write-Output "Document indexed."
}
catch {
    Write-Error "Unable to index the document.. "
    Write-Error $_
    #save the enum locally as a .json if this indexing fails.
    $enum | convertto-json -depth 40 | out-file "{0} - {1} - Domain.json" -f $data.timestamp,$pc
}